var express = require('express');
var router = express.Router();
const {
  getName,
  getAllUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser
} = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");


router.get('/', function (req, res, next) {
  res.send(`Welcome!`);
});


/**
 * Get an array of all users
 */
router.get('/user', isAuthorized, function (req, res, next) {
  const result = getAllUsers();

  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`Error in request body!`);
  }
});

/**
 * Get one user by ID
 */
router.get('/user/:id', function (req, res, next) {
  const result = getUserById(req.params.id);

  if (result) {
    res.send(result);
  } else {
    res.status(404).send(`Cant find user with this ID!`);
  }
});


/**
 * Create user according to the data from the request body
 */
router.post('/user', function (req, res, next) {
  const result = updateUser(req.body);

  if (result) {
    res.send(`Your name is ${result}`);
  } else {
    res.status(400).send(`User unauthorized!`);
  }
});


/**
 * Update user according to the data from the request body
 */
router.put('/user/:id', function (req, res, next) {
  const result = createUser(res.body);

  if (result) {
    res.send(result);
  } else {
    res.status(500).send('Cant create new user.');
  }
});


/**
 * Delete one user by ID
 */
router.delete('/user/:id', function (req, res, next) {
  const result = deleteUser(res.params.id);

  if (result) {
    res.send(result);
  } else {
    res.status(404).send('Cant find user with this ID.');
  }
});

module.exports = router;
