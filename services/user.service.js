const { 
  create,
  getAll,
  getByID,
  getByName,
  saveData,
  removeByID,
  removeAll 
} = require("../repositories/user.repository");

const getName = (user) => {
  if (user) {
    return user.name;
  } else {
    return null;
  }
};

const getAllUsers = () => {
  return getAll();
};

const getUserById = (id) => {
  return getByID(id);
};

const createUser = (userData) => {
  return create(userData);
};

const updateUser = (data) => {
  if(data) {
    return saveData(data);
  } else {
    return null;
  }
};

const deleteUser = (id) => {
  return removeByID(id);
};

module.exports = {
  getName,
  getAllUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser
};