const users = require('../data/users.jsons');
const { cloneObj, guid } = require('../utils');

const create = (user) => {
  const id = guid();

  Object.assign(user, { id });

  users.push(user);

  return true;
}

const getAll = () => {
  if (Array.isArray(users)) {
    return cloneObj(users);
  } else {
    return [];
  }
}

const getByID = (id) => {
  if (!Array.isArray(users)) return;

  return users.find(user => user._id === id);
}
const getByName = (name) => {
  if (!Array.isArray(users)) return;

  return users.find(user => user.name === name);
}

const saveData = (data) => {
  const { id } = data;
  const user = getByID(id);

  Object.assign(user, data);

  if (user) {
    console.log(`${data} is saved`);
    return user;
  } else {
    return false;
  }
}

const removeByID = (id) => {
  const ind = users.findIndex(user => user._id === id);
  const res = users.splice(ind, 1);

  if (res) {
    return true;
  } else {
    return false;
  }
}

const removeAll = () => {
  users = [];
  return true;
}

module.exports = {
  create,
  getAll,
  getByID,
  getByName,
  saveData,
  removeByID,
  removeAll
};